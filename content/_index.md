---
start: Start
all: All
floss: FLOSS Events
kde: KDE Events
meetings: Meetings
releases: Releases
events:
  - year: 1969
    entries:
    - category: floss-events
      title: UNIX is born
      image:
        - url: "https://www.flickr.com/photos/darthpedrius/6846503675/sizes/o/in/photostream/"
          src: "/images/Thompson_Ritchie.jpg"
          caption: Thompson & Ritchie
      content: >-
        In 1969, [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) and [Dennis Ritchie](https://en.wikipedia.org/wiki/Dennis_Ritchie) started working on [UNIX](http://www.unix.org/what_is_unix/history_timeline.html). Initially written in assembler, it was soon rewritten in [C](https://en.wikipedia.org/wiki/C_(programming_language)), a language created by Ritchie and considered high level.
  - year: 1979
    entries:
    - category: floss-events
      title: C++ was created
      image:
        - url: "https://en.wikipedia.org/wiki/Bjarne_Stroustrup"
          src: "/images/BjarneStroustrup.jpg"
          caption: Bjarne Stroustrup
      content: >-
        In 1979, [Bjarne Stroustrup](http://www.stroustrup.com/bio.html) started developing "C with classes", which would later become the [C++](http://www.softwarepreservation.org/projects/c_plus_plus/). In his opinion, it was the only language of the time that allowed to write programs that were at the same time efficient and elegant.
  - year: 1984
    entries:
    - category: floss-events
      title: The beginning of Free Software
      image:
        - url: "https://en.wikipedia.org/wiki/Richard_Stallman"
          src: "/images/richard_stallman.jpg"
          caption: Richard Stallman
      content: >-
        In 1984, [Richard Stallman](https://stallman.org/biographies.html#serious) started developing [GNU](https://www.gnu.org/gnu/about-gnu.html) (GNU is Not Unix), a completely free operating system based on Unix, which was proprietary.
  - year: 1991
    entries:
    - category: floss-events
      title: The Linux Kernel
      image:
        - url: "https://www.oregonlive.com/business/index.ssf/2012/06/linus_torvalds_shares_2012_mil.html"
          src: "/images/linus.jpg"
          caption: Linus Torvalds
      content: >-
        In 1991, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds) created the [Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel) based on [MINIX](http://www.minix3.org/), a version of Unix written by [Andrew Tanenbaum](http://www.cs.vu.nl/~ast/home/faq.html). The emergence of Linux has revolutionized the history of free software and helped to popularize it. See the [25 Years of Linux Kernel Development infographic](https://web.archive.org/web/20190628003733im_/https://www.linux.com/sites/lcom/files/styles/rendered_file/public/linux-kernel-development-infographic-2016.jpg?itok=DqVqiplt).
  - year: 1993
    entries:
    - category: floss-events
      title: The first distros are born
      image:
        - url: "/images/gnulinuxLogo.png"
          src: "/images/gnulinuxLogo.png"
          caption: GNU & Tux
      content: >-
        In 1993, the first free distributions begin to emerge, based in [GNU](https://en.wikipedia.org/wiki/GNU) and [Linux](https://en.wikipedia.org/wiki/Linux). A GNU/Linux distribution is usually formed by the Linux kernel, GNU tools and libraries, and more a collection of applications.
  - year: 1995
    entries:
    - category: floss-events
      title: Qt was created
      image:
        - url: "https://en.wikipedia.org/wiki/Qt_(software)"
          src: "/images/qt-logo.png"
          caption: Qt logo
      content: >-
        In 1995, the Norwegian company Troll Tech created the cross-platform framework [Qt](https://wiki.qt.io/About_Qt), with which KDE would be created in the following year. Qt became the basis of the main KDE technologies in these 20 years. Learn more about [Qt History](https://wiki.qt.io/Qt_History).
  - year: 1996
    entries:
    - category: kde-events
      title: KDE was announced
      image:
        - url: "https://www.linuxjournal.com/article/6834"
          src: "/images/Matthias.jpg"
          caption: Matthias Ettrich
      content: >-
        In 1996, [Matthias Ettrich](http://www.linuxjournal.com/article/6834) announced the creation of Kool Desktop Environment (KDE), a graphical interface for Unix systems, built with Qt and C ++ and designed for the end user. The name "KDE" was a pun on the graphic environment [CDE](https://en.wikipedia.org/wiki/Common_Desktop_Environment), which was proprietary at the time. Read [the original announcement](https://www.kde.org/announcements/announcement.php) of the KDE Project.
  - year: 1997
    entries:
    - category: meetings
      title: KDE One Conference
      image:
        - src: "/images/kde-one-arnsberg.png"
          caption: Cornelius Schumacher's Archive
      content: >-
        In 1997, about 15 KDE developers met in Arnsberg, Germany, to work on the project and discuss its future. This event became known as [KDE One](https://community.kde.org/KDE_Project_History/KDE_One_(Developer_Meeting)).
    - category: releases
      title: KDE Beta 1
      image:
        - url: "/images/KDEBeta1.gif"
          src: "/images/KDEBeta1.gif"
          caption: KDE Beta 1 Screenshot
      content: >-
        The beta 1 version of KDE was [released](https://www.kde.org/announcements/beta1announce.php) exactly 12 months after the project announcement. The release text emphasized that KDE was not a window manager, but an integrated environment in which the window manager was just another part.
    - category: kde-events
      title: KDE e.V. was founded
      image:
        - url: "https://ev.kde.org/images/ev_large.png"
          src: "/images/ev_large.png"
          caption: KDE e.V. logo
      content: >-
        In 1997, [KDE e.V.](https://ev.kde.org/), the nonprofit that represents the KDE community financially and legally, was founded in Tübingen, Germany.
  - year: 1998
    entries:
    - category: kde-events
      title: KDE Free Qt Foundation was created
      image:
        - url: "https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237"
          src: "/images/kde-qt.png"
          caption: Konqi with Qt in its heart
      content: >-
        The foundation agreement for the [KDE Free Qt Foundation](https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php) is signed by KDE e.V. and Trolltech, then owner of Qt. The Foundation [ensures the permanent availability](https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open--legal-update/) of Qt as Free Software.
    - category: releases
      title: KDE 1 was released
      image:
        - url: "https://www.betaarchive.com/imageupload/2014-12/1419415455.or.26035.png"
          src: "/images/KDE1.png"
          caption: KDE 1
      content: >-
        KDE released the [first stable version](https://www.kde.org/announcements/announce-1.0.php) of its graphical environment in 1998, with highlights as an application development framework, the KOM/OpenParts, and a preview of its office suite.
  - year: 1999
    entries:
    - category: kde-events
      title: Konqi
      image:
        - url: "https://upload.wikimedia.org/wikipedia/commons/0/0a/Konqi-klogo-official-400x500_b.png"
          src: "/images/Konqi.png"
          caption: Konqi
      content: >-
        In April 1999, a dragon is announced as the new animated assistant to the KDE Help Center. It was so charming that it replaced the previous project mascot, Kandalf, from version 3.x on. See the [KDE 2 Screenshot](/images/kde2b3_5.png) showing Konqi and Kandalf.
    - category: meetings
      title: KDE Two Conference
      image:
        - src: "/images/kde-two-erlangen.jpg"
          caption: Group Photo (Cornelius Schumacher's Archive)
      content: >-
        In October 1999, the second meeting of KDE developers took place in Erlangen, Germany. Read the [report](https://community.kde.org/KDE_Project_History/KDE_Two_(Developer_Meeting)) on the KDE Two Conference.
  - year: 2000
    entries:
    - category: releases
      title: "KDE Desktop"
      image:
        - url: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/KDE_2_logo.svg/500px-KDE_2_logo.svg.png"
          src: "/images/KDE2logo.png"
          caption: KDE 2 logo
      content: >-
        From the [beta 1 version of KDE 2](https://www.kde.org/announcements/announce-1.90.php) it is possible to perceive a project naming change. The releases that once referred to the project as "K Desktop Environment", began referring to it only as "KDE Desktop".
    - category: meetings
      title: KDE Three Beta Conference
      image:
        - src: "/images/kde-three-beta-meeting.jpg"
          caption: Cornelius Schumacher's Archive
      content: >-
        In July 2000, the third meeting (beta) of KDE developers occurred in Trysil, Norway. [Find out](https://community.kde.org/KDE_Project_History/KDE_Three_Beta_(Developer_Meeting)) what was done during the conference.
    - category: releases
      title: KDE 2 was released
      image:
        - url: "/images/kde2b3_6.png"
          src: "/images/kde2.png"
          caption: KDE 2
      content: >-
        KDE [released](https://kde.org/announcements/1-2-3/2.0/) its second version, featuring as main news [Konqueror](https://konqueror.org/) web browser and file manager; and the office suite [KOffice](https://en.wikipedia.org/wiki/KOffice). KDE had its code almost entirely rewritten for this second version.
  - year: 2001
    entries:
    - category: releases
      title: "KDE Project"
      image:
        - url: "https://www.kde.org/stuff/clipart/splashscreen-2.1-400x248.png"
          src: "/images/splashscreen-2.1.png"
          caption: KDE 2.1 Splashscreen
      content: >-
        From the [release announcement](https://www.kde.org/announcements/announce-2.1.2.php) of version 2.1.2 there is also a change of the nomenclature. The announcements began referring to KDE as "KDE Project".
    - category: kde-events
      title: KDE Women
      image:
        - url: "https://www.kde.org/stuff/clipart/katie-221x223.jpg"
          src: "/images/katie.png"
          caption: Katie, Konqi's girlfriend
      content: >-
        In March 2001, the creation of community women's group was announced. The [KDE Women](https://community.kde.org/KDE_Women) aimed to help increase the number of women in free software communities, particularly in KDE. Watch the video ["Highlights of KDE Women"](https://www.youtube.com/watch?v=HTwQ-oGTmGA) of the Akademy 2010.
  - year: 2002
    entries:
    - category: meetings
      title: KDE Three Meeting
      image:
        - src: "/images/ThreeMeeting.jpg"
          caption: Group Photo of KDE Three
      content: >-
        In March 2002, about 25 developers gathered for the [third KDE meeting](https://community.kde.org/KDE_Project_History/KDE_Three_(Developer_Meeting)) in Nuremberg, Germany. KDE 3 was about to be released and the KDE 2 code needed to be migrated to the new library Qt 3.
    - category: releases
      title: KDE 3
      image:
        - url: "/images/kde300-snapshot2-1152x864.jpg"
          src: "/images/kde3.jpg"
          caption: KDE 3
      content: >-
        KDE released its [third version](https://kde.org/announcements/1-2-3/3.0/), showing as important additions a new print framework, KDEPrint; the translation of the project for 50 languages; and a package of educational applications, maintained by the KDE Edutainment Project.
    - category: kde-events
      title: KDE e.V. meeting
      image:
        - src: "/images/kde-ev-meeting.jpg"
          caption: Group Photo (Cornelius Schumacher's Archive)
      content: >-
        In August 2002, there was a [meeting of board members of the KDE e.V.](https://ev.kde.org/reports/2002.php) that was essential to establish how the organization works. At this meeting it was decided, among other things, that the brand "KDE" would be registered and that new members should be invited and supported by two active members of e.V..
  - year: 2003
    entries:
    - category: releases
      title: KDE 3.1
      image:
        - url: "/images/3.1-8.png"
          src: "/images/kde31.png"
          caption: KDE 3.1
      content: >-
        In [version 3.1](https://kde.org/announcements/1-2-3/3.1/) the community presented KDE with a new look, a new theme for widgets, called Keramik, and Crystal as default theme for the icons. See [KDE 3.1 New Feature Guide](https://kde.org/info/1-2-3/3.1/feature_guide_1.html).
    - category: meetings
      title: Kastle
      image:
        - url: "https://devel-home.kde.org/~duffus/akademy/2003/groupphoto/NoveHradykde3.2beta.jpg"
          src: "/images/Kastle.jpg"
          caption: Group Photo of Kastle
      content: >-
        In August 2003, about 100 contributors of KDE from various countries gathered in a castle in the Czech Republic. The event was called [Kastle](https://akademy.kde.org/2003) and was the forerunner of Akademy, the event that would become the international annual meeting of the community.
  - year: 2004
    entries:
    - category: meetings
      title: Akademy 2004
      image:
        - url: "https://devel-home.kde.org/~duffus/akademy/2004/groupphoto/7781f1.jpg"
          src: "/images/akademy-2004.jpg"
          caption: Group Photo of Akademy 2004
      content: >-
        In August 2004, the [first international meeting of the community](https://conference2004.kde.org/) took place. The event was held in Ludwigsburg, Germany, and launched a series of international events called ["Akademy"](https://akademy.kde.org/) which take place annually since then. The event got its name because it happened in the "Filmakademie" city film school. See the [group photos](https://devel-home.kde.org/~duffus/akademy/) of all Akademies.
  - year: 2005
    entries:
    - category: releases
      title: KDE 3.5
      image:
        - url: "/images/3.5-35-superkaramba.png"
          src: "/images/kde35.png"
          caption: KDE 3.5
      content: >-
        [KDE 3.5 was released](https://www.kde.org/announcements/announce-3.5.php). This version introduced several new features, among them, the SuperKaramba, a tool that allowed customize your desktop with "applets"; the Amarok and Kaffeine players; and the media burner K3B. See [KDE 3.5: A Visual Guide to New Features](https://www.kde.org/announcements/visualguide-3.5.php).
  - year: 2006
    entries:
    - category: meetings
      title: First Akademy-Es
      image:
        - url: "https://i.imgur.com/Le0ZAIf.jpg"
          src: "/images/akademy-es.jpg"
          caption: First Akademy-es photo
      content: >-
        In March 2006, the [first meeting](https://dot.kde.org/2006/02/10/akademy-es-2006-barcelona) of Spanish KDE contributors took place in Barcelona. Since then, Akademy-Es has turned into an annual event. Learn more about [Spanish KDE contributors group](https://www.kde-espana.org/).
    - category: meetings
      title: KDE Four Core meeting
      image:
        - src: "/images/kde-four-core-meeting.jpg"
          caption: Group Photo (Cornelius Schumacher's Archive)
      content: >-
        In July 2006, the developers of the KDE core libraries gathered in Trysill, Norway, for the [KDE Four Core meeting](https://dot.kde.org/2006/06/26/kde-libs-hackers-meet-kde-four-core). The event was a kind of successor to the KDE Beta Three Conference and KDE Three Meeting and in it developers worked on the development of KDE 4 and stabilization of some core libraries to the project.
  - year: 2007
    entries:
    - category: meetings
      title: Guademy
      image:
        - url: "https://static.kdenews.org/dannya/GuademyArticle_group_picture.jpg"
          src: "/images/Guademy.jpg"
          caption: Group Photo of Guademy
      content: >-
        In March 2007, several contributors of KDE and [Gnome](https://www.gnome.org/) met in A Coruña, Spain, an event which sought to establish a collaboration between the two projects. The event became known as [Guademy](https://dot.kde.org/2007/03/28/guademy-2007-event-report), a mixture of [Guadec](https://wiki.gnome.org/GUADEC), the name given to the Gnome event with Akademy, KDE event name.
    - category: releases
      title: KDE 4 Alpha 1
      image:
        - url: "https://kde.org/announcements/4/4.0-alpha1-visual-guide/desktop.png"
          src: "/images/kde4alpha.png"
          caption: KDE 4 Alpha 1
      content: >-
        In May 2007, [the alpha 1 version of KDE 4](https://www.kde.org/announcements/announce-4.0-alpha1.php), codenamed "Knut", was announced. This announcement showed a completely new desktop, with a new theme, Oxygen, new applications like Okular and Dolphin, and a new desktop shell, Plasma. See [KDE 4.0 Alpha 1: A Visual Guide to New Features](https://www.kde.org/announcements/visualguide-4.0-alpha1.php).
    - category: releases
      title: KDE 4 Development Platform
      image:
        - url: "https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237"
          src: "/images/konqi-dev.png"
          caption: Konqi developer
      content: >-
        In October 2007, KDE announced the [release candidate](https://www.kde.org/announcements/announce-4.0-platform-rc1.php) of its development platform consisting of basic libraries and tools to develop KDE applications.
  - year: 2008
    entries:
    - category: releases
      title: KDE 4
      image:
        - url: "https://kde.org/announcements/4/4.0/dolphin-systemsettings-kickoff.png"
          src: "/images/kde4.jpg"
          caption: KDE 4.0
      content: >-
        In 2008, the community [announced the revolutionary KDE 4](https://kde.org/announcements/4/4.0/). In addition to the visual impact of the new default theme, Oxygen, and the new desktop interface, Plasma; KDE 4 also innovated by presenting the following applications: the PDF reader Okular, the Dolphin file manager, as well as KWin, supporting graphics effects. See [KDE 4.0 Visual Guide](https://kde.org/announcements/4/4.0/guide/).
    - category: meetings
      title: "KDE Community"
      image:
        - url: "https://cibermundi.files.wordpress.com/2016/07/mascotes.png"
          src: "/images/mascotes.png"
          caption: Konqis community
      content: >-
        From the [announcement of version 4.1](https://www.kde.org/announcements/4.1/) on there was already a tendency to refer to KDE as a "community" and not just as a "project". This change was recognized and affirmed in the rebranding announcement of the following year.
  - year: 2009
    entries:
    - category: meetings
      title: First Camp KDE
      image:
        - url: "https://c2.staticflickr.com/4/3406/3219319008_806bfdb8b5_b.jpg"
          src: "/images/campkde.jpg"
          caption: Group Photo of 2009
      content: >-
        In January 2009, [the first edition of Camp KDE](https://techbase.kde.org/Events/CampKDE/2009) took place in Negril, Jamaica. It was the first KDE event in the Americas. After that, there were two more US-based conferences, [in 2010 in San Diego](https://dot.kde.org/2010/01/17/day-one-camp-kde-2010), and another [in 2011 in San Francisco](https://dot.kde.org/2011/04/13/re-live-camp-kde-experience).
    - category: meetings
      title: Gran Canaria Desktop Summit
      image:
        - url: "https://devel-home.kde.org/~duffus/akademy/2009/groupphoto/"
          src: "/images/akademy-2009.jpg"
          caption: Group Photo of DS 2009
      content: >-
        In July 2009, the first [Desktop Summit](http://www.grancanariadesktopsummit.org/), a joint conference of the KDE and Gnome communities, took place in Gran Canaria, Spain. The [Akademy 2009](https://dot.kde.org/2009/07/14/vibrant-community-propels-kde-forward-akademy-2009) was held with this event.
    - category: kde-events
      title: 1 million of commits
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png"
          src: "/images/active-contributors.png"
          caption: Active contributors at the time
      content: >-
        The community [reached the mark of 1 million of commits](https://dot.kde.org/2009/07/20/kde-reaches-1000000-commits-its-subversion-repository). From 500,000 in January 2006 and 750,000 in December 2007, only 19 months later, contributions reached the 1 million mark. The increase in these contributions coincides with the launch of innovative KDE 4.
    - category: meetings
      title: First Randa Meetings
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/t3-groupphoto.jpg"
          src: "/images/randameetings.jpg"
          caption: Group photo of first RM
      content: >-
        In September 2009, [the first](https://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds) of a series of events known as [Randa Meetings](https://community.kde.org/Sprints/Randa) took place in Randa, in Swiss Alps. The event brought together several sprints of various community projects. Since then, Randa Meetings take place annually.
    - category: kde-events
      title: Rebranding
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/brandmap.png"
          src: "/images/brandmap.png"
          caption: Brand graph
      content: >-
        In November 2009, the community [announced](https://dot.kde.org/2009/11/24/repositioning-kde-brand) changes to its brand. The name "K Desktop Environment" had become ambiguous and obsolete and is replaced by "KDE". The name "KDE" is no longer just referring to a desktop environment, but now represents both the community and the project umbrella supported by this community.
    - category: kde-events
      title: KDE Software Compilation
      image:
        - url: "https://cibermundi.files.wordpress.com/2016/08/1024px-kde_brand_map-svg.png"
          src: "/images/kde_brand_map.png"
          caption: Brand Map
      content: >-
        From [version 4.3.4](https://www.kde.org/announcements/announce-4.3.4.php) on, KDE announcements began to refer to the whole suite of products as 'KDE Software Compilation' (KDE SC). Currently, this trend is abandoned.
  - year: 2010
    entries:
    - category: meetings
      title: Akademy-Br
      image:
        - url: "https://cibermundi.files.wordpress.com/2010/04/img_0086.jpeg"
          src: "/images/akademy-br.jpeg"
          caption: Group Photo of Akademy-Br
      content: >-
        In April 2010, [the first meeting](http://ev.kde.org/reports/ev-quarterly-2010Q2.pdf) of Brazil KDE contributors took place. The event was held in Salvador, Bahia, and was the only Brazilian Akademy edition. From 2012 on, the event expanded to a meeting for all Latin American contributors.
    - category: kde-events
      title: Join the Game
      image:
        - url: "https://kde.org/announcements/4/4.9.0/images/join-the-game.png"
          src: "/images/join-the-game.png"
          caption: JtG logo
      content: >-
        In June 2010, the KDE e.V. announced the supporting membership program ["Join the Game"](https://jointhegame.kde.org/), which aims to encourage financial support to the community. By participating in the program you become a member of the KDE e.V., contributing to an annual amount and being able participate in the organization's annual meetings.
    - category: releases
      title: KDE SC 4.5
      image:
        - url: "https://kde.org/announcements/4/4.5.0/plasma-netbook-sal.png"
          src: "/images/plasma-netbook.png"
          caption: Plasma Netbook screenshot
      content: >-
        In August 2010, the community [announced version 4.5](https://www.kde.org/announcements/4.5/) of its products: Development Platform, Applications and Plasma Workspaces. Each of them began to have a separate release announcement. One of the highlights of this version was the Plasma interface for netbooks, announced in version 4.4.
    - category: releases
      title: Calligra Suite
      image:
        - url: "/images/banner_calligra.png"
          src: "/images/banner_calligra.png"
          caption: Calligra Suite logo
      content: >-
        In December 2010, the community [announces](https://dot.kde.org/2010/12/06/kde-announces-calligra-suite) the [Calligra Suite](https://www.calligra.org/), a split from the KOffice suite. KOffice was discontinued in 2011.
  - year: 2011
    entries:
    - category: meetings
      title: First Conf KDE India
      image:
        - url: "https://www.flickr.com/photos/jriddell/5519171984/in/photostream/lightbox/"
          src: "/images/conf_kde_india.jpg"
          caption: Group Photo of Conf India
      content: >-
        In March 2011, the [first conference](https://dot.kde.org/2010/12/28/confkdein-first-kde-conference-india) of the KDE and Qt community in India took place in Bengaluru. Since then, the event has taken place annually.
    - category: meetings
      title: Desktop Summit 2011
      image:
        - url: "https://devel-home.kde.org/~duffus/akademy/2011/groupphoto/"
          src: "/images/desktop-summit.jpg"
          caption: Group Photo of DS 2011
      content: >-
        In August 2011, [another joint conference](https://desktopsummit.org/) of the KDE and Gnome communities took place in Berlin, Germany. Nearly 800 contributors from all over the world came together to share ideas and collaborate on various free software projects.
    - category: releases
      title: Plasma Active
      image:
        - url: "https://kde.org/announcements/plasma-mobile/plasma-active-one/activity.png"
          src: "/images/plasma_active.png"
          caption: Plasma Active 1 screenshot
      content: >-
        The community released the first version of its interface for mobile devices, [Plasma Active](https://www.kde.org/announcements/plasma-active-one/). Later, it was superseded by Plasma Mobile.
  - year: 2012
    entries:
    - category: meetings
      title: First LaKademy
      image:
        - url: "https://blog.filipesaraiva.info/wp-content/uploads/2012/04/531194_331401383593730_100001716125440_793041_189106717_n.jpg"
          src: "/images/lakademy.jpg"
          caption: Group Photo of LaKademy 2012
      content: >-
        In April 2012, the [first meeting](https://lakademy.kde.org/lakademy12-en.html) of the KDE contributors in Latin America, LaKademy, took place. The event was held in Porto Alegre, Brazil. The [second edition](https://br.kde.org/lakademy-2014) took place in 2014 in São Paulo, and since then has been an annual event. So far, all editions were held in Brazil, where the highest number of contributors from the Latin American community is based.
    - category: kde-events
      title: KDE Manifesto
      image:
        - url: "https://manifesto.kde.org/images/tree.png"
          src: "/images/tree.png"
          caption: KDE Manifesto Art
      content: >-
        [KDE Manifesto](https://manifesto.kde.org/index.html), a document that presents the benefits and obligations of a KDE project, was released. It also introduces the core values that guide the community: Open Governance, Free Software, Inclusivity, Innovation, Common Ownership, and End-User Focus.
    - category: kde-events
      title: New Konqi
      image:
        - url: "https://www.deviantart.com/tysontan/art/Konqi-ver-2-494267237"
          src: "/images/mascot_konqi.png"
          caption: Konqi redesigned
      content: >-
        In December 2012, the community [launched a competition](https://dot.kde.org/2012/12/08/contest-create-konqi-krita) to create a new mascot using Krita. The competition winner was Tyson Tan, who created [new looks for the Konqi and Katie](http://tysontan.deviantart.com/art/Konqi-ver-2-494267237).
  - year: 2013
    entries:
    - category: kde-events
      title: Release cycle change
      image:
        - url: "https://blog.jospoortvliet.com/2014/11/where-is-kde-5-and-when-can-i-use-it.html"
          src: "/images/KDE5.png"
          caption: Graph of the KDE technologies separated
      content: >-
        In September 2013, the community [announced](https://dot.kde.org/2013/09/04/kde-release-structure-evolves) changes in the release cycle of its products. Each of them, Workspaces, Applications, and Platform, now have separate releases. The change was already a reflection of the restructuring of KDE technologies. This restructuring resulted in the next generation of community products, which would be released the following year.
  - year: 2014
    entries:
    - category: releases
      title: Frameworks 5
      image:
        - url: "https://en.wikipedia.org/wiki/KDE_Frameworks_5#/media/File:Evolution_and_development_of_KDE_software.svg"
          src: "/images/Evolution_KDE.png"
          caption: Evolution of development of KDE technologies
      content: >-
        The first stable version of Frameworks 5 (KF5), the successor of KDE Platform 4, [was released](https://www.kde.org/announcements/kde-frameworks-5.0.php). This new generation of the KDE libraries based on Qt 5 has made the KDE development platform more modular and facilitated cross-platform development.
    - category: releases
      title: Plasma 5
      image:
        - url: "https://kde.org/announcements/plasma/5/5.0/screenshots/desktop.png"
          src: "/images/plasma5.png"
          caption: Plasma 5 screenshot
      content: >-
        [Release](https://www.kde.org/announcements/plasma5.0/) of the first stable version of Plasma 5. This new generation of Plasma has a new theme, Breeze. Changes include a migration to a new, fully hardware-accelerated graphics stack centered around an OpenGL(ES) scenegraph. This version of Plasma uses as base the Qt 5 and Frameworks 5.
    - category: kde-events
      title: GCompris joins the KDE
      image:
        - url: "/images/gcompris-logo.png"
          src: "/images/gcompris-logo.png"
          caption: GCompris logo
      content: >-
        In December 2014, the educational software suite [GCompris joins](https://dot.kde.org/2014/12/11/gcompris-joins-kde-incubator-and-launches-fundraiser) the [project incubator of KDE community](https://community.kde.org/Incubator). Bruno Coudoin, who created the [project](http://gcompris.net/index-en.html) in 2000, decided to rewrite it in Qt Quick to facilitate its use on mobile platforms. It was originally written in GTK+.
    - category: releases
      title: Plasma Mobile
      image:
        - url: "https://i.ytimg.com/vi/auuQA0Q8qpM/maxresdefault.jpg"
          src: "/images/plasma-mobile.jpg"
          caption: Plasma Mobile photo
      content: >-
        The community [announced Plasma Mobile](https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform), an interface for smartphones that uses Qt, Frameworks 5 and Plasma Shell technologies.
  - year: 2015
    entries:
    - category: releases
      title: Plasma on Wayland
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/kwin_screenshot_cH1426-wee.png"
          src: "/images/plasma-wayland.png"
          caption: KWin on Wayland
      content: >-
        The [first live image](https://dot.kde.org/2015/12/18/first-plasma-wayland-live-image) of Plasma running on Wayland was made available for download. Since 2011, the community works in support of Wayland by KWin, the Compositor of Plasma, and the Window Manager.
    - category: releases
      title: Plasma 5.5
      image:
        - url: "https://kde.org/announcements/plasma/5/5.5.0/discover.png"
          src: "/images/plasma5-5.png"
          caption: Plasma 5.5 screenshot
      content: >-
        Version 5.5 is [announced](https://www.kde.org/announcements/plasma-5.5.0.php) with several new features: new icons added to the Breeze theme, support for OpenGL ES in KWin, progress to support Wayland, a new default font (Noto), a new design.
  - year: 2016
    entries:
    - category: kde-events
      title: KDE Neon
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/installer.png"
          src: "/images/neon.png"
          caption: Neon 5.6 screenshot
      content: >-
        The community [announced](https://dot.kde.org/2016/01/30/fosdem-announcing-kde-neon) the inclusion of another project in its incubator, [KDE Neon](https://neon.kde.org/), based on Ubuntu. Developers, testers, artists, translators and early adopters can get fresh code from git as it is committed by the KDE community.
    - category: kde-events
      title: Akademy 2016 part of QtCon
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/QtConInfo_v4_wee.png"
          src: "/images/QtCon.png"
          caption: QtCon banner
      content: >-
        [Akademy 2016](https://akademy.kde.org/2016) took place as a part of [QtCon](http://qtcon.org/) in September 2016 in Berlin, Germany. The event brought together the Qt, [FSFE](https://fsfe.org/), [VideoLAN](http://www.videolan.org/) and KDE communities. It celebrated 20 years of KDE, 20 years of VLC, and 15 years of FSFE.
    - category: releases
      title: Kirigami UI
      image:
        - url: "https://dot.kde.org/sites/dot.kde.org/files/kirigami.png"
          src: "/images/kirigami.png"
          caption: Kirigami logo
      content: >-
        [Kirigami is released](https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui), a set of QML components to develop applications based on Qt for mobile or desktop devices.
    - category: kde-events
      title: KDE presents its Vision for the future
      image:
        - src: "/images/vision_collage.jpg"
          caption: 
      content: >-
        In early 2016, as a result of a survey and open discussions among community members, [KDE has published a document outlining its vision for the future](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future). This vision represents the values that its members consider most important: "A world in which everyone has control over their digital life and enjoys freedom and privacy." The idea in defining this vision was to make clear what are the main motivations that drive the community.
    - category: kde-events
      title: KDE announced the Advisory Board
      image:
        - src: "/images/advisory_board.jpg"
          caption: 
      content: >-
        In order to formalize the cooperation among the community and the organizations that have been its allies, [KDE e.V. announced the Advisory Board](https://dot.kde.org/2016/09/26/announcing-kde-advisory-board). Through the Advisory Board, organizations can provide feedback on community activities and decisions, to participate in regular meetings with KDE e.V., and to attend to Akademy and community sprints.
    - category: kde-events
      title: KDE celebrated 20 years
      image:
        - src: "/images/kde20_anos.png"
          caption: KDE 20 years art by Elias Silveira
      content: >-
        On October 14, KDE celebrated its 20th birthday. The project that started as a desktop environment for Unix systems, today is a community that incubates ideas and projects which go far beyond desktop technologies. To celebrate its anniversary the community [published a book](https://20years.kde.org/book/) written by its contributors. [We also had parties](https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary) been held in eight countries.
  - year: 2017
    entries:
    - category: kde-events
      title: KDE Slimbook is announced
      image:
        - src: "/images/kde_slimbook.jpg"
          caption: KDE Slimbook
      content: >-
        In partnership with a Spanish laptop retailer [the community announced the launch of the KDE Slimbook](https://dot.kde.org/2017/01/26/kde-and-slimbook-release-laptop-kde-fans), a ultrabook that comes with the KDE Plasma and KDE Applications pre-installed. The laptop offers a pre-installed version of KDE Neon and [can be purchased from the retailer's website](https://slimbook.es/en/store/slimbook-kde).
    - category: kde-events
      title: QtCon Brasil is announced
      image:
        - src: "/images/qtconbr.png"
          caption: 
      content: >-
        Inspired by QtCon 2016, which took place in Berlin and brought together the KDE, VLC, Qt and FSFE communities, [KDE community in Brazil hosted QtCon Brasil in 2017](https://br.qtcon.org/2017/). The event was held in São Paulo and brought together Qt experts from Brazil and abroad in two days of talks and one day of training.
    - category: kde-events
      title: KDE set goals
      image:
        - src: "/images/kde_goals.jpg"
          caption: 
      content: >-
        [KDE sets its goals for the next four years](https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond). As part of an effort undertaken by its members since 2015, the community has defined three main objectives for the coming years: to improve the usability and productivity of its software, to ensure that its software helps to preserve the privacy of users and to facilitate the contribution and integration of new collaborators.
  - year: 2018
    entries:
    - category: kde-events
      title: Handshake Foundation donates USD 300,000 to KDE
      image:
        - src: "/images/handshake.jpg"
          caption: Handshake Foundation
      content: >-
        In 2018, the [Handshake Foundation made a donation of USD 300,000 to KDE e.V](https://dot.kde.org/2018/10/15/kde-ev-receives-sizeable-donation-handshake-foundation). The money was used to finance projects, fund the development of Calligra Office Suite and to continue our commitment to keep creating free and open source software for users worldwide.
    - category: kde-events
      title: Debian Joins KDE Advisory Board
      image:
        - src: "/images/debian_community.jpg"
          caption: Debian Community
      content: >-
        Debian, the famous Linux distribution founded in 1993, [joined KDE's Advisory Board](https://dot.kde.org/2018/07/12/debian-joins-kdes-advisory-board) to further deepen the collaboration between both communities. With this, we expect both communities to grow even stronger and to keep working together to build FLOSS projects.
    - category: kde-events
      title: The Pineapple fund donates USD 200,000 to KDE
      image:
        - src: "/images/pineapple.jpg"
          caption: Pineapple Fund
      content: >-
        Pineapple, an anonymous philanthropic fund, [donated USD 200,000 to KDE e.V](https://dot.kde.org/2018/02/19/kde-receives-200000-usd-donation-pineapple-fund) in 2018. This allowed KDE to keep paying for all the expenses related to our community, such as sprints and events.
  - year: 2019
    entries:
    - category: kde-events
      title: KDE-powered newspaper achieves world first
      image:
        - src: "/images/newspaper.jpg"
          caption:
      content: >-
        The *Janayugom* daily newspaper becomes the [first in the world](https://poddery.com/posts/4691002) to switch to a completely free software stack for its publication. The regional paper has 100,000 subscribers across Kerala, India. An event celebrating the move is attended by the state's Chief Minister. Plasma, Okular, Krita, and more are deployed on systems across 14 different offices.
    - category: kde-events
      title: KDE adopts GitLab as an online development platform
      image:
        - src: "/images/gitlab_kde.png"
          caption: KDE adopts GitLab
      content: >-
        In 2019, [KDE transitioned to GitLab](https://about.gitlab.com/press/releases/2019-09-17-gitlab-adopted-by-KDE.html) as our main DevOps platform. This change allowed the KDE community to work more efficiently and to lower the bar for new contributors to join us. Our GitLab instance can be found [on KDE Invent](https://invent.kde.org).
    - category: kde-events
      title: New goals for 2019
      image:
        - src: "/images/goals-2019.jpg"
          caption:
      content: >-
        KDE [announces](https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps) a new slate of goals for the near future at Akademy 2019. The community has whittled down  a long list of proposals and chose to prioritize a consistent user experience across all KDE software, getting KDE apps into the hands of users with the help of new packaging methods, and improving the Wayland session.
    - category: kde-events
      title: KDE adopts Matrix as an Instant Messaging Platform
      image:
        - src: "/images/neochat.png"
          caption:
      content: >-
        [Matrix](https://community.kde.org/Matrix), an Open Source, decentralized and secure Instant Messaging platform, became the default and recommended way of communication for those who like their instant messaging graphical. Matrix allows community members to use a web-based, desktop or mobile clients, as well as native KDE clients, such as [Neochat](https://invent.kde.org/network/neochat). IRC channels still exist and are bridged to Matrix, ensuring that members using different systems can communicate easily with each other. Likewise Telegram channels, although these are not considered official, as Telegram does not open source the code for its servers or encryption methods.
  - year: 2020
    entries:
    - category: kde-events
      title: KDE PinePhone Announced
      image:
        - src: "/images/Pinephone_1000.jpg"
          caption: KDE PinePhone
      content: >-
        KDE's Plasma Mobile developers team up with [PINE64](https://www.pine64.org/) to create the [PinePhone KDE Community Edition](https://www.pine64.org/pinephone/), a mobile phone that runs solely Free Software, is easy to hack and protects your privacy.
    - category: kde-events
      title: GCompris celebrates its 20th Anniversary and is deployed in thousands of schools in Kerala, India
      image:
        - src: "/images/gcompris_in.png"
          caption: GCompris in Malayalam
      content: >-
        In 2020 GCompris celebrated its 20th birthday and was also [deployed in many schools in Kerala, India](https://timotheegiet.com/blog/floss/tales-of-the-kde-network-kerala.html). KDE educational applications have been helping students around the world for decades and are part of the [KDE Educational Project](https://edu.kde.org).
  - year: 2021
    entries:
    - category: kde-events
      title: Slimbook Becomes a KDE Patron
      image:
        - src: "/images/kde_slimbook_patron.jpg"
          caption: Alejandro López (right), CEO at Slimbook, with Adriaan de Groot from the KDE e.V. board.
      content: >-
        Slimbook, a Linux-focused Spanish computer brand, [has joined KDE as a Patron](https://dot.kde.org/2021/02/25/slimbook-becomes-kde-patron). They have been offering a variety of computers with GNU/Linux and Plasma for years, and continue to support KDE and open source with their contributions.
    - category: kde-events
      title: PINE64 joins KDE as a Patron
      image:
        - src: "/images/pine_kde.png"
          caption: PinePhone - KDE Community Edition
      content: >-
        PINE64 is a company that aims to deliver affordable, high-quality phones and other devices with open source software. [PINE64 has become a KDE Patron](https://dot.kde.org/2021/06/02/pine64-becomes-kde-patron) after years of shipping devices with KDE software.
    - category: kde-events
      title: TUXEDO Computers Becomes a KDE Patron
      image:
        - src: "/images/tuxedo_kde.jpg"
          caption: Herbert Feiler, CEO of TUXEDO Computers.
      content: >-
        TUXEDO Computers offers a variety of products with GNU/Linux and KDE Plasma pre-installed. They have been supporting open source software for years and now [they have become KDE Patrons](https://dot.kde.org/2021/09/14/tuxedo-computers-becomes-newest-kde-patron).
    - category: kde-events
      title: Valve chooses KDE Plasma for Steam Deck
      image:
        - src: "/images/kde_deck.jpg"
          caption: KDE Plasma running on Steam Deck
      content: >-
        Valve, the creators of Steam, announced their portable gaming computer called Steam Deck has KDE Plasma as the default desktop experience. KDE developers have been working with Valve to make Plasma work well with Steam Deck.
  - year: 2022
    entries:
    - category: kde-events
      title: First in-person post-Pandemic Akademy held in Barcelona
      image:
        - src: "/images/000_Group_photo_straight.jpg"
          caption: Attendees to the 2022 Akademy held in Barcelona.
      content: >-
        Akademy 2022 was held in Barcelona from the 1st to the 7th of October. it was the first time in two years Community members and guests laid out in person what had been going on within KDE's projects (and adjacent projects), the state of the art, and where things were headed.
    - category: kde-events
      title: KDE runs two consecutive fundraisers and completes them both
      image:
        - src: "/images/konqi-katie-donation_1500.jpg"
          caption: Katie and Konqi thank you for your donation.
      content: >-
         KDE attempted two new fundraising initiatives in 2022. The first involved [raising funds for a specific app](https://kdenlive.org/en/fund/): Kdenlive, KDE's video editor. Our goal was to raise €15,000 to enable developers to add much-requested features and stabilize the code. The second initiative was [the end-of-year fundraiser](https://kde.org/fundraisers/yearend2022/), where we aimed to raise €20,000. Both fundraisers were massive successes, surpassing the goals we had set. This achievement stands as a testament to the generosity of community members, supporters, and users.
  - year: 2023
    entries:
    - category: kde-events
      title: The KDE Free Qt Foundation celebrates its 25th anniversary
      image:
        - src: "/images/hero_smaller_bust.png"
          caption: The KDE Free Qt Foundation helps keep the Qt toolset free.
      content: >-
        [The Foundation](https://kde.org/community/whatiskde/kdefreeqtfoundation/) guarantees the present and future freedom of the Qt framework as it has the right to release Qt under the BSD license if necessary to ensure that Qt remains open source. This remarkable legal guarantee protects the free software community and creates trust among developers, contributors, and customers.
    - category: kde-events
      title: g10 Code Becomes a KDE Patron
      image:
        - src: "/images/g10_logo.png"
          caption: The g10 logo.
      content: >-
         [g10 Code GmbH joined the ranks of KDE patrons](https://dot.kde.org/2023/04/25/g10-code-becomes-kde-patron). g10 Code provides custom development, enhancements, and audits of cryptographic software -- in particular for the popular GnuPG encryption and digital signature tools.
  - year: 2024
    entries:
    - category: kde-events
      title: KDE Launches Plasma 6
      image:
        - src: "/images/Plasma6_screenshot.png"
          caption: The default Plasma 6 desktop environment.
      content: >-
        With Plasma 6, our technology stack underwent two major upgrades: a transition to the latest version of our application framework, Qt, and a migration to the modern Linux graphics platform, Wayland. The launch was dubbed "MegaRelease", as new versions of KDE's apps and Frameworks, along with a new version of Plasma Mobile were all published at the same time.
---
