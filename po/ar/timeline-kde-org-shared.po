#
# SPDX-FileCopyrightText: 2025 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: websites-timeline-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2025-02-21 11:50+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.5\n"

#: i18n/en.yaml:0
msgid "Your names"
msgstr ""

#: i18n/en.yaml:0
msgid "Translated by"
msgstr "المترجمون"

#: i18n/en.yaml:0
msgid "KDE Timeline"
msgstr "الخط الزمني لكِيدِي"

#. %1 number of years
#: i18n/en.yaml:0
msgid "A %1 year timeline of KDE events"
msgstr "خط زمني مدته %1 عامًا لأحداث كيدي"

#: i18n/en.yaml:0
msgid "Languages"
msgstr "اللغات"

#: i18n/en.yaml:0
msgid "Select your language"
msgstr "اختر لغتك"

#. %1 number of years
#: i18n/en.yaml:0
msgid "%1 Years of KDE"
msgstr "%1 سنة من كيدي"

#. %1 number of years
#: i18n/en.yaml:0
msgid ""
"A tour through the moments that marked the %1 years of community history, "
"starting with the technologies that made possible its existence. See our KDE "
"20 years <a style=\"text-decoration:underline;\" href=\"https://20years.kde."
"org/book/index.html\">book</a>."
msgstr ""

#: i18n/en.yaml:0
msgid ""
"KDE<sup>®</sup> and [the K Desktop Environment<sup>®</sup> logo](https://kde."
"org/media/images/trademark_kde_gear_black_logo.png) are registered "
"trademarks of [KDE e.V.](https://ev.kde.org/ \"Homepage of the KDE non-"
"profit Organization\")"
msgstr ""
"الشعار KDE<sup>®</sup> و الشعار [the K Desktop Environment<sup>®</sup> ]"
"(https://kde.org/media/images/trademark_kde_gear_black_logo.png) علامتان "
"مسجلتان لـ [كِيدِي e.V.](https://ev.kde.org/ \"Homepage of the KDE non-profit "
"Organization\")"

#: i18n/en.yaml:0
msgid "Legal"
msgstr "قانوني"

#: i18n/en.yaml:0
msgid ""
"Found a bug? <a href=\"mailto:kde-www@kde.org\" target=\"_blank\">E-mail us</"
"a>"
msgstr ""

#: i18n/en.yaml:0
msgid "Tweet"
msgstr "غرد"

#: i18n/en.yaml:0
msgid "Available in:"
msgstr "متوفّرة في:"
